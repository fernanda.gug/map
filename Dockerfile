
FROM gcc:latest
WORKDIR /matrice
COPY p1.cpp /matrice/
RUN gcc -o p1 p1.cpp -lstdc++
CMD ["./p1"]
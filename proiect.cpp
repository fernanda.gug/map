#include <iostream>
#include <cstdlib>
#include <ctime>
#include<algorithm>
using namespace std;

int main() 
{

    const int M = 3; 
    const int N = 3;  

    srand(time(nullptr));


    int matrice[M][N];
    for (int i = 0; i < M; i++) 
    {
        for (int j = 0; j < N; j++) 
        {
            matrice[i][j] = rand() % 10 + 1;  
        }
    }


    cout << "Matricea generata:" << endl;
    for (int i = 0; i < M; i++) 
    {
        for (int j = 0; j < N; j++)
        {
            cout << matrice[i][j] << " ";
        }
        cout << endl;
    }


    int sumaDiagonalaPrincipala = 0;
    int sumaDiagonalaSecundara = 0;

    for (int i = 0; i < M; i++) 
    {
        sumaDiagonalaPrincipala += matrice[i][i];
        sumaDiagonalaSecundara += matrice[i][N - 1 - i];
    }

    cout << " Suma elementelor de pe diagonala principala: " << sumaDiagonalaPrincipala << endl;
    cout << " Suma elementelor de pe diagonala secundara: " << sumaDiagonalaSecundara << endl;
    cout << endl;


    int minimSubDiagonalaPrincipala = matrice[1][0];
    int maximSubDiagonalaPrincipala = matrice[1][0];
    int minimSubDiagonalaSecundara = matrice[M - 2][N - 1];
    int maximSubDiagonalaSecundara = matrice[M - 2][N - 1];

    for (int i = 1; i < M; i++) 
    {
        for (int j = 0; j < i; j++) 
        {
            minimSubDiagonalaPrincipala = min(minimSubDiagonalaPrincipala, matrice[i][j]);
            maximSubDiagonalaPrincipala = max(maximSubDiagonalaPrincipala, matrice[i][j]);
        }
    }

    for (int i = 0; i < M - 1; i++) 
    {
        for (int j = N - 2; j > i; j--)
        {
            minimSubDiagonalaSecundara = min(minimSubDiagonalaSecundara, matrice[i][j]);
            maximSubDiagonalaSecundara = max(maximSubDiagonalaSecundara, matrice[i][j]);
        }
    }

    cout << " Minimul elementelor sub diagonala principala: " << minimSubDiagonalaPrincipala << endl;
    cout << " Maximul elementelor sub diagonala principala: " << maximSubDiagonalaPrincipala << endl;
    cout << " Minimul elementelor sub diagonala secundara: " << minimSubDiagonalaSecundara << endl;
    cout << " Maximul elementelor sub diagonala secundara: " << maximSubDiagonalaSecundara << endl;

    return 0;
}
